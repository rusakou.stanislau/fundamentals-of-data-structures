package Task_1;

import java.util.Arrays;

public class CustomArrayList {

    private static final int DEFAULT_SIZE = 10;

    private int[] elements;
    private int currentIndex;

    public CustomArrayList() {
        elements = new int[DEFAULT_SIZE];
    }

    public void add(int element) {
        if (currentIndex >= elements.length) {
            doubleArraySize();
        }

        elements[currentIndex++] = element;
    }

    public void add(int element, int index) {
        indexValidation(index);

        if (currentIndex >= elements.length) {
            doubleArraySize();
        }

        System.arraycopy(elements, index, elements, index + 1, elements.length - currentIndex);

        elements[index] = element;
        currentIndex++;
    }

    private void doubleArraySize() {
        int newSize = elements.length * 2;
        int[] newElements = new int[newSize];
        System.arraycopy(elements, 0, newElements, 0, elements.length);
        elements = newElements;
    }

    public void removeByIndex(int index) {
        indexValidation(index);

        int[] elementsToKeep = new int[currentIndex--];
        System.arraycopy(elements, 0, elementsToKeep, 0, index);
        System.arraycopy(elements, index + 1, elementsToKeep, index, elementsToKeep.length - index);
        elements = elementsToKeep;
    }

    public void removeByValue(int value) {
        int index = 0;

        for (int element : elements) {
            if (value == element) {
                removeByIndex(index);
            } else {
                index++;
            }
        }
    }

    public void replace(int element, int index) {
        indexValidation(index);

        elements[index] = element;
    }

    private void indexValidation(int index) {
        if (index >= elements.length - 1) {
            throw new ArrayIndexOutOfBoundsException("Current size: " + elements.length + ", index: " + index);
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(elements);
    }
}
